// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "TFHC/WaterRaining"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_Specular("Specular", Range( 0 , 1)) = 0
		_Color0("Color 0", Color) = (0.5807742,0.7100198,0.9632353,0)
		_Smothness("Smothness", Range( 0 , 1)) = 0
		_Albedo("Albedo", 2D) = "white" {}
		_NormalMap("Normal Map", 2D) = "bump" {}
		_RainDrops("RainDrops", 2D) = "bump" {}
		_WaterNormal("Water Normal", 2D) = "bump" {}
		_WetMask("WetMask", 2D) = "white" {}
		_RainSpeed("Rain Speed", Range( 0 , 50)) = 0
		_WetAmount("Wet Amount", Range( 0 , 1)) = 0
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf StandardSpecular keepalpha addshadow fullforwardshadows vertex:vertexDataFunc
		struct Input
		{
			float2 uv_NormalMap;
			float2 texcoord_0;
			float2 texcoord_1;
			float2 uv_WetMask;
			float2 uv_Albedo;
		};

		uniform sampler2D _NormalMap;
		uniform sampler2D _RainDrops;
		uniform float4 _RainDrops_ST;
		uniform float _RainSpeed;
		uniform sampler2D _WaterNormal;
		uniform float4 _WaterNormal_ST;
		uniform sampler2D _WetMask;
		uniform float _WetAmount;
		uniform float4 _Color0;
		uniform sampler2D _Albedo;
		uniform float _Specular;
		uniform float _Smothness;

		void vertexDataFunc(inout appdata_full vertexData, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.texcoord_0.xy = vertexData.texcoord.xy * _RainDrops_ST.xy + _RainDrops_ST.zw;
			o.texcoord_1.xy = vertexData.texcoord.xy * _WaterNormal_ST.xy + _WaterNormal_ST.zw;
		}

		void surf( Input input , inout SurfaceOutputStandardSpecular output )
		{
			// *** BEGIN Flipbook UV Animation vars ***
			// Total tiles of Flipbook Texture
			float fbtotaltiles4 = 8.0 * 8.0;
			// Offsets for cols and rows of Flipbook Texture
			float fbcolsoffset4 = 1.0f / 8.0;
			float fbrowsoffset4 = 1.0f / 8.0;
			// Speed of animation
			float fbspeed4 = _Time[1] * abs(_RainSpeed);
			// UV Tiling (col and row offset)
			float2 fbtiling4 = float2(fbcolsoffset4, fbrowsoffset4);
			// UV Offset - calculate current tile linear index, and convert it to (X * coloffset, Y * rowoffset)
			// Calculate current tile linear index
			int fbcurrenttileindex4 = (int)fmod( fbspeed4 , fbtotaltiles4);
			// Obtain Offset X coordinate from current tile linear index
			int fblinearindextox4 = fbcurrenttileindex4 % (int)8.0;
			// Multiply Offset X by coloffset
			float fboffsetx4 = fblinearindextox4 * fbcolsoffset4;
			// Obtain Offset Y coordinate from current tile linear index
			int fblinearindextoy4 = (int)( ( fbcurrenttileindex4 - fblinearindextox4 ) / 8.0 ) % (int)8.0;
			// Reverse Y to get tiles from Top to Bottom
			fblinearindextoy4 = (int)8.0 - fblinearindextoy4;
			// Multiply Offset Y by rowoffset
			float fboffsety4 = fblinearindextoy4 * fbrowsoffset4;
			// UV Offset
			float2 fboffset4 = float2(fboffsetx4, fboffsety4);
			// Flipbook UV
			half2 fbuv4 = float4( float2( frac( input.texcoord_0.x ) , frac( input.texcoord_0.y ) ), 0.0 , 0.0 ) * fbtiling4 + fboffset4;
			// *** END Flipbook UV Animation vars ***
			float4 floatToFloat4_960=_WetAmount;
			float4 Node96Port0FLOAT4=( tex2D( _WetMask,input.uv_WetMask) * floatToFloat4_960 );
			output.Normal = lerp( UnpackNormal( tex2D( _NormalMap,input.uv_NormalMap) ) , ( UnpackNormal( tex2D( _RainDrops,fbuv4) ) + UnpackNormal( tex2D( _WaterNormal,frac(abs( input.texcoord_1+_Time[0] * float2(1,1 )))) ) ) , Node96Port0FLOAT4.x );
			output.Albedo = ( lerp( float4( 1,1,1,0 ) , _Color0 , Node96Port0FLOAT4.x ) * tex2D( _Albedo,input.uv_Albedo) ).xyz;
			float4 floatToFloat4_1350=_Specular;
			output.Specular = clamp( ( Node96Port0FLOAT4 * floatToFloat4_1350 ) , float4( 0,0,0,0 ) , float4( 1,1,1,1 ) ).xyz;
			float4 floatToFloat4_1340=_Smothness;
			output.Smoothness = clamp( ( Node96Port0FLOAT4 * floatToFloat4_1340 ) , float4( 0,0,0,0 ) , float4( 1,0,0,0 ) ).x;
		}

		ENDCG
	}
	Fallback "Diffuse"
}
/*ASEBEGIN
Version=2404
204;94;916;471;1028.969;809.8573;2.860114;True;False
Node;AmplifyShaderEditor.FractNode;73;-393.5435,-245.1878;Float;FLOAT;0.0
Node;AmplifyShaderEditor.FractNode;74;-388.0634,-164.3565;Float;FLOAT;0.0
Node;AmplifyShaderEditor.AppendNode;75;-248.3211,-224.6375;Float;FLOAT2;0;0;0;0;FLOAT;0.0;FLOAT;0.0;FLOAT;0.0;FLOAT;0.0
Node;AmplifyShaderEditor.TFHCFlipBookUVAnimation;4;-129.6723,-197.1499;Float;0;0;FLOAT2;0,0;FLOAT;8.0;FLOAT;8.0;FLOAT;0.0
Node;AmplifyShaderEditor.RangedFloatNode;6;-384.1099,-57.41589;Float;Property;_RainSpeed;Rain Speed;60;0;0;50
Node;AmplifyShaderEditor.TimeNode;108;-591.7221,78.02065;Float
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;121;689.9142,-326.7747;Float;COLOR;0.0,0,0,0;FLOAT4;0,0,0,0
Node;AmplifyShaderEditor.LerpOp;120;582.7883,-538.2794;Float;COLOR;1,1,1,0;COLOR;1,0,0,0;FLOAT4;0.0,0,0,0
Node;AmplifyShaderEditor.RangedFloatNode;95;-211.5786,359.5932;Float;Property;_WetAmount;Wet Amount;60;0;0;1
Node;AmplifyShaderEditor.PannerNode;91;-223.5844,34.66089;Float;1;1;FLOAT2;0,0;FLOAT;0.0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;817.1534,-131.4963;Float;True;2;Float;StandardSpecular;TFHC/WaterRaining;False;False;False;False;False;False;False;False;False;False;False;False;Back;On;LEqual;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;FLOAT3;0,0,0;FLOAT3;0,0,0;FLOAT3;0,0,0;FLOAT3;0,0,0;FLOAT;0.0;FLOAT;0.0;FLOAT;0.0;OBJECT;0.0;OBJECT;0.0;OBJECT;0.0;OBJECT;0.0;FLOAT3;0,0,0
Node;AmplifyShaderEditor.SimpleAddOpNode;126;434.033,-104.1853;Float;FLOAT3;0.0,0,0;FLOAT3;0,0,0
Node;AmplifyShaderEditor.LerpOp;88;606.7914,-126.7868;Float;FLOAT3;0.0,0,0;FLOAT3;0,0,0;FLOAT4;0.0,0,0,0
Node;AmplifyShaderEditor.SamplerNode;102;43.26754,-20.25338;Float;Property;_WaterNormal;Water Normal;40;None;True;0;True;bump;Auto;True;Object;-1;FLOAT2;0,0;FLOAT;1.0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;135;365.977,252.182;Float;FLOAT4;0.0,0,0,0;FLOAT;0.0
Node;AmplifyShaderEditor.SamplerNode;113;-311.2887,176.2322;Float;Property;_WetMask;WetMask;50;None;True;0;False;white;Auto;False;Object;-1;FLOAT2;0,0;FLOAT;1.0
Node;AmplifyShaderEditor.ColorNode;119;291.0571,-699.5067;Float;Property;_Color0;Color 0;-1;0.5807742,0.7100198,0.9632353,0
Node;AmplifyShaderEditor.SamplerNode;66;-5.855574,-610.3518;Float;Property;_Albedo;Albedo;10;None;True;0;False;white;Auto;False;Object;-1;FLOAT2;0,0;FLOAT;1.0
Node;AmplifyShaderEditor.SamplerNode;11;68.51689,-175.2399;Float;Property;_RainDrops;RainDrops;30;None;True;0;True;bump;Auto;True;Object;-1;FLOAT2;0,0;FLOAT;1.0
Node;AmplifyShaderEditor.SamplerNode;137;14.59595,-361.821;Float;Property;_NormalMap;Normal Map;20;None;True;0;True;bump;Auto;True;Object;-1;FLOAT2;0,0;FLOAT;1.0
Node;AmplifyShaderEditor.TextureCoordinatesNode;40;-660.33,-253.9992;Float;0;3;FLOAT2;1,1;FLOAT2;0,0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;96;37.23515,172.3491;Float;FLOAT4;0.0,0,0,0;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;140;481.7873,112.6552;Float;FLOAT4;0.0,0,0,0;FLOAT4;0,0,0,0;FLOAT4;1,1,1,1
Node;AmplifyShaderEditor.ClampOpNode;141;621.9305,203.5172;Float;FLOAT4;0.0,0,0,0;FLOAT4;0,0,0,0;FLOAT4;1,0,0,0
Node;AmplifyShaderEditor.RangedFloatNode;127;36.06594,305.8419;Float;Property;_Specular;Specular;-1;0;0;1
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;134;546.6888,374.0211;Float;FLOAT4;0.0,0,0,0;FLOAT;0.0
Node;AmplifyShaderEditor.TextureCoordinatesNode;104;-621.9628,-76.47506;Float;0;0;FLOAT2;1,1;FLOAT2;0,0
Node;AmplifyShaderEditor.RangedFloatNode;125;95.10609,384.4607;Float;Property;_Smothness;Smothness;-1;0;0;1
WireConnection;73;0;40;1
WireConnection;74;0;40;2
WireConnection;75;0;73;0
WireConnection;75;1;74;0
WireConnection;4;0;75;0
WireConnection;4;3;6;0
WireConnection;121;0;120;0
WireConnection;121;1;66;0
WireConnection;120;1;119;0
WireConnection;120;2;96;0
WireConnection;91;0;104;0
WireConnection;91;1;108;1
WireConnection;0;0;121;0
WireConnection;0;1;88;0
WireConnection;0;3;140;0
WireConnection;0;4;141;0
WireConnection;126;0;11;0
WireConnection;126;1;102;0
WireConnection;88;0;137;0
WireConnection;88;1;126;0
WireConnection;88;2;96;0
WireConnection;102;0;91;0
WireConnection;135;0;96;0
WireConnection;135;1;127;0
WireConnection;11;0;4;0
WireConnection;96;0;113;0
WireConnection;96;1;95;0
WireConnection;140;0;135;0
WireConnection;141;0;134;0
WireConnection;134;0;96;0
WireConnection;134;1;125;0
ASEEND*/
//CHKSM=1EA4FAABF870A3918B6B0723111C24589F9EB8E4