﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretFire : MonoBehaviour
{
    ParticleSystem particles;

    [SerializeField]
    string targetTag;

    [SerializeField]
    int m_Damage = 40;

    // Start is called before the first frame update
    void Start()
    {
        particles = GetComponent<ParticleSystem>();     
    }

    // Update is called once per frame
    void Update()
    {
        if (!particles.isPlaying)
        {
            Destroy(gameObject);
        }
    }

    //deal damage on collision
    void OnParticleCollision(GameObject other)
    {
        Health health = other.GetComponent<Health>();
        if (health && other.CompareTag(targetTag))
        {
            health.DoDamage(m_Damage);
        }
    }
}
