﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBullets : MonoBehaviour
{
    [SerializeField]
    GameObject gunGO;

    GunLogic gun;

    [SerializeField]
    float permanentDuration = 10.0f;

    public bool explosive = false;

    private float duration = 10.0f;

    //a particle system which acts as ui showing player the staus of the power up
    [SerializeField]
    GameObject particleUI;

    // Start is called before the first frame update
    void Start()
    {
        duration = permanentDuration;
        gun = gunGO.GetComponent<GunLogic>();
    }

    // Update is called once per frame
    void Update()
    {
        //set gun to use explosive bullets for duration of powerup
        if (explosive)
        {
            gun.explosive = true;
            duration -= Time.deltaTime;
            if (!particleUI.activeSelf)
            {
                particleUI.SetActive(true);
            }
        }
        if (duration <= 0)
        {
            explosive = false;
            gun.explosive = false;
            duration = permanentDuration;
            if (particleUI.activeSelf)
            {
                particleUI.SetActive(false);
            }
        }

    }
}
