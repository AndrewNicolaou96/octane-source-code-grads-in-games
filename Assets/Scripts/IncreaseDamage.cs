﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseDamage : MonoBehaviour
{
    [SerializeField]
    GameObject bulletPrefab;

    BulletLogic bullet;

    [SerializeField]
    float slowDownFactor = 0.1f;

    [SerializeField]
    float permanentDuration = 10.0f;

    public bool increasedDamage;

    private float duration = 10.0f;

    [SerializeField]
    int standardDamage = 50;

    [SerializeField]
    int higherDamage = 100;

    //a particle system which acts as ui showing player the staus of the power up
    [SerializeField]
    GameObject particleUI;


    // Start is called before the first frame update
    void Start()
    {
        bullet = bulletPrefab.GetComponent<BulletLogic>();
        duration = permanentDuration;
    }

    // Update is called once per frame
    void Update()
    {
        if (increasedDamage)
        {
            bullet.m_Damage =  higherDamage;
            duration -= Time.deltaTime / Time.timeScale;
            if (!particleUI.activeSelf)
            {
                particleUI.SetActive(true);
            }
        }
        else
        {
            bullet.m_Damage = standardDamage;
            if (particleUI.activeSelf)
            {
                particleUI.SetActive(false);
            }
        }
        if (duration <= 0)
        {
            increasedDamage = false;
            bullet.m_Damage = standardDamage;
            duration = permanentDuration;
        }

    }
}
