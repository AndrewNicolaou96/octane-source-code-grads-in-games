﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeckAttack : MonoBehaviour
{
    [SerializeField]
    GameObject player;
    PlayerController playerController;
    Health playerHealth;

    [SerializeField]
    int damage;

    DragonAI dragonAI;

    // Start is called before the first frame update
    void Start()
    {
        playerController = player.GetComponent<PlayerController>();
        playerHealth = player.GetComponent<Health>();
        dragonAI = GetComponentInParent<DragonAI>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("collision");
        if (other.gameObject.CompareTag("Player") && dragonAI.isAttacking)
        {
                playerController.AddForce(new Vector3(0, 50, 0));
                Debug.Log("collision");
                playerHealth.DoDamage(damage);
        }
    }
}
