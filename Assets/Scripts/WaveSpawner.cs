﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{
    //enemies spawned per spawner
    int numberOFEnemies;
    int waveNumber = 1;

    //PLACES WHERE NPCS WILL SPAWN
    NpcSpawner[] spawners;

    //no enemies left
    bool waveOver;

    //if the update method is waiting for invoke to run
    bool isWaitingforInvoke;

    [SerializeField]
    GameObject player;

    Health playerHealth;

    //screen that disaplays when health is 0
    [SerializeField]
    GameObject gameOverScreen;

    //container for the powerups
    [SerializeField]
    GameObject powerUps;

    //current wave displayed on screen
    [SerializeField]
    Text waveNumberText;
    // Start is called before the first frame update
    void Start()
    {
        playerHealth = player.GetComponent<Health>();
        spawners = FindObjectsOfType<NpcSpawner>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerHealth.getValue() <= 0)
        {
            gameOver();
        }

        //loop through spawners to see if any still have enemies 
        waveOver = true;
        foreach (NpcSpawner spawner in spawners) {
            if (spawner.getNpcCount() > 0)
            {
                waveOver = false;
            }
        }

        //spawn new wave in 5 seconds
        if (waveOver && FindObjectsOfType<AIController>().Length <= 0 && !isWaitingforInvoke)
        {
            Invoke("newWave", 5.0f);
            isWaitingforInvoke = true;
        }
    }

    void newWave()
    {
        waveNumber++;
        foreach (NpcSpawner spawner in spawners)
        {
            spawner.setNpcCount(waveNumber);
            spawner.setCoolDown(5f/waveNumber);
        }
        waveNumberText.text = "Wave Number: " + waveNumber.ToString();
        waveOver = false;
        isWaitingforInvoke = false;
        //set all children of powerups to active
        for(int i = 0; i < powerUps.transform.childCount; i++)
        {
            powerUps.transform.GetChild(i).gameObject.SetActive(true);
        }
        powerUps.SetActive(true);
    }

    public void gameOver()
    {
        Time.timeScale = 0f;
        gameOverScreen.SetActive(true);
    } 
}
