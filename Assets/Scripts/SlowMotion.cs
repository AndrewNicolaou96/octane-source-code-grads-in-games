﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMotion : MonoBehaviour
{
    [SerializeField]
    GameObject player;

    PlayerController playerController;

    [SerializeField]
    float slowDownFactor = 0.1f;

    [SerializeField]
    float permanentDuration = 10.0f;

    AudioSource[] allAudio;
    bool audioPitchShifted = false;

    //if slomo effect should be applied
    public bool slowMo;

    private float duration = 10.0f;

    //a particle system which acts as ui showing player the staus of the power up
    [SerializeField]
    GameObject particleUI;

    // Start is called before the first frame update
    void Start()
    {
        duration = permanentDuration;
        allAudio = FindObjectsOfType<AudioSource>();
        playerController = player.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (slowMo)
        {
            playerController.slowMo = true;
            Time.timeScale = slowDownFactor;
            duration -= Time.deltaTime /Time.timeScale;
            //slowmo music and sfx
            if (!audioPitchShifted)
            {
                foreach(AudioSource a in allAudio)
                {
                    if (a)
                    {
                        a.pitch = a.pitch * slowDownFactor;
                    }
                }
                audioPitchShifted = true;
                if (!particleUI.activeSelf)
                {
                    particleUI.SetActive(true);
                }
            }
        }
        //effect lasts for a set duration then all is set to default
        if (duration <= 0)
        {
            Debug.Log("duration met");
            slowMo = false;
            playerController.slowMo = false;
            Time.timeScale = 1.0f;
            duration = permanentDuration;
            foreach (AudioSource a in allAudio)
            {
                if (a)
                {
                    a.pitch = a.pitch / slowDownFactor;
                }
            }
            audioPitchShifted = false;
            if (particleUI.activeSelf)
            {
                particleUI.SetActive(false);
            }
        }
        
    }
}
