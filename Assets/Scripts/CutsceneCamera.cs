﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneCamera : MonoBehaviour
{
    [SerializeField]
    Transform player;

    //where the camera is looking
    [SerializeField]
    Transform focus;

    //where the camera should be
    Transform anchor;

    //where the camera should be looking
    Transform focusAnchor;

    //if the camera position is locked
    bool locked;

    //if the camera focus position is locked 
    bool lockedfocus;

    [SerializeField]
    float cameraMoveSpeed = 15;

    //how fast the focus point of the camera moves towares the focus anchor
    [SerializeField]
    float focusMoveSpeed = 30;

    // Start is called before the first frame update
    void Start()
    {
        focus.position = player.position;
    }

    // Update is called once per frame
    void Update()
    {
        float distanceToMove = 0;
        Vector3 direction = Vector3.zero;

        if (!locked)
        {
            //move camera to anchor point
            distanceToMove = cameraMoveSpeed * Time.deltaTime;
            direction = anchor.position - transform.position;
            if (Vector3.Distance(anchor.position, transform.position) > 0.5f)
            {
                transform.Translate(direction.normalized * distanceToMove, Space.World);
            }
            else
            {
                locked = true;
            }
        }
        else
        {
            transform.position = anchor.position;
        }

        //Move focus point to focus anchor
        distanceToMove = focusMoveSpeed * Time.deltaTime;
        direction = focusAnchor.position - focus.position;
        if (!lockedfocus)
        {
            if (Vector3.Distance(focusAnchor.position, focus.position) > 0.5f)
            {
                focus.Translate(direction.normalized * distanceToMove, Space.World);
            }
            else
            {
                lockedfocus = true;
            }
        }
        else
        {
            focus.position = focusAnchor.position;
        }


        //look at the focus point as it travels towards the anchor 
        transform.LookAt(focus);
    }

    private void OnDisable()
    {
        //return focus to the player
        focus.position = player.position;
    }

    public void setCutscene(Transform anchor, Transform focusAnchor)
    {
        this.anchor = anchor;
        this.focusAnchor = focusAnchor;
    }
}
