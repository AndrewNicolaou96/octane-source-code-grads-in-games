﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//dragon fireball attack
public class FireballExplosion : MonoBehaviour
{
    //damage area
    ParticleSystem AOErange;
    [SerializeField]

    GameObject Exlposion;
    GameObject player;
    Health playerHealth;
    PlayerController playerController;

    bool isQuitting;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        AOErange = GetComponent<ParticleSystem>();
        playerHealth = player.GetComponent<Health>();
        playerController = player.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!AOErange.isPlaying)
        {
            Destroy(gameObject);
        }
    }

    private void OnApplicationQuit()
    {
        isQuitting = true;
    }

    private void OnDestroy()
    {
        if (!isQuitting)
        {
            //cause damage  to player
            Instantiate(Exlposion, transform.position, transform.rotation);
            if (Vector3.Distance(player.transform.position, transform.position) < 4.0f)
            {
                if (player != null)
                {
                    playerHealth.DoDamage(50);
                    playerController.AddForce(new Vector3(0, 50, 0));
                }
            }
        }
    }


}
