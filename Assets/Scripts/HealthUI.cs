﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class HealthUI : MonoBehaviour
{
    private PostProcessVolume postprocessingVolume;
    private float vignetteIntensity;
    private Vignette vignette;
    private AudioLowPassFilter lowPassFilter;

    //sound frequencies to cut off as health gets lower
    int cutOffFrequency;

    [SerializeField]
    GameObject player;

    private Health playerHealth;

    // Start is called before the first frame update
    void Start()
    {
        lowPassFilter = GetComponent<AudioLowPassFilter>();
        playerHealth = player.GetComponent<Health>();
        vignette = ScriptableObject.CreateInstance<Vignette>();
        vignette.enabled.Override(true);
        vignette.intensity.Override(1f);
        postprocessingVolume = GetComponent<PostProcessVolume>();
    }

    // Update is called once per frame
    void Update()
    {

        //make vignette more intense as health gets lower
        if(playerHealth.getValue() < 50)
        {
            vignetteIntensity = 1f - (playerHealth.getValue()*2 / 100f);//vignette
            cutOffFrequency = 0 + (playerHealth.getValue() * 100) * 2; //muffled frequencies
            lowPassFilter.cutoffFrequency = cutOffFrequency;
            lowPassFilter.enabled = true;

        }
        else
        {
            vignetteIntensity = 0f;
            lowPassFilter.enabled = false;
        }
        vignette.intensity.value = vignetteIntensity;
        //set the vignette intesity in the post process volume
        postprocessingVolume = PostProcessManager.instance.QuickVolume(gameObject.layer, 100f, vignette);
    }
}
