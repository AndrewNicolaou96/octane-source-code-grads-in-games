﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaGlow : MonoBehaviour
{
    [SerializeField]
    Transform player;
    [SerializeField]
    Transform dragon;

    Light lava;
    // Start is called before the first frame update
    void Start()
    {
        lava = GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        //increase lava glow as player gets closer to dragon
        float distance = Vector3.Distance(player.position, dragon.position);
        if (distance < 1000f)
        {
            lava.intensity = 1- distance/1000f; //increase intensity up to 1 by a factor of the distance
        }
        else
        {
            lava.intensity = 0;

        }
    }
}
