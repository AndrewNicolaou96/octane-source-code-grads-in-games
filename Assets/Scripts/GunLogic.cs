﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunLogic : MonoBehaviour
{
    // The Bullet Prefab
    [SerializeField]
    GameObject m_BulletPrefab;

    // The Explosive Bullet Prefab
    [SerializeField]
    GameObject m_ExplosiveBulletPrefab;

    // The Bullet Spawn Point
    [SerializeField]
    Transform m_BulletSpawnPoint;

    // VFX
    [SerializeField]
    ParticleSystem m_Flare;

    [SerializeField]
    ParticleSystem m_Smoke;

    [SerializeField]
    ParticleSystem m_Sparks;

    // SFX
    [SerializeField]
    AudioClip m_BulletShot;

    [SerializeField]
    AudioClip m_GrenadeLaunched;

    // The AudioSource to play Sounds for this object
    AudioSource m_AudioSource;
    private PlayerController playerController;

    //if the bullet instatiated should be normal or explosive
    public bool explosive = false;

    // Use this for initialization
    void Start()
    {
        m_AudioSource = GetComponent<AudioSource>();
        playerController = FindObjectOfType<PlayerController>().GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!playerController.isPaused)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                if (!explosive)
                {
                    Fire();
                }
                else
                {
                    FireGrenade();

                }
            }
        }
    }

    void Fire()
    {
        if (m_BulletPrefab)
        {
            // Create the Projectile from the Bullet Prefab
            Instantiate(m_BulletPrefab, m_BulletSpawnPoint.position, transform.rotation * m_BulletPrefab.transform.rotation);

            // Play Particle Effects
            PlayGunVFX();

            // Play Sound effect
            if (m_AudioSource && m_BulletShot)
            {
                m_AudioSource.PlayOneShot(m_BulletShot);
            }

        }
    }

    void FireGrenade()
    {
        if (m_ExplosiveBulletPrefab)
        {
            // Create the Projectile from the Explosive Bullet Prefab
            Instantiate(m_ExplosiveBulletPrefab, m_BulletSpawnPoint.position, transform.rotation * m_ExplosiveBulletPrefab.transform.rotation);

            // Play Particle Effects
            PlayGunVFX();

            // Play Sound effect
            if (m_AudioSource && m_GrenadeLaunched)
            {
                m_AudioSource.PlayOneShot(m_GrenadeLaunched);
            }
        }
    }

    void PlayGunVFX()
    {
        if (m_Flare)
        {
            m_Flare.Play();
        }

        if (m_Sparks)
        {
            m_Sparks.Play();
        }

        if (m_Smoke)
        {
            m_Smoke.Play();
        }
    }
}


