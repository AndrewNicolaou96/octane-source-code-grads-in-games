﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOnTrigger : MonoBehaviour
{
    [SerializeField]
    GameObject m_ObjectToBeActivated;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            m_ObjectToBeActivated.SetActive(true);
        }
    }
}
