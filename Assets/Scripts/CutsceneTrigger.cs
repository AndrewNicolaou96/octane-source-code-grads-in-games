﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneTrigger : MonoBehaviour
{
    [SerializeField]
    Camera cam;

    [SerializeField]
    Transform cameraPosition;

    [SerializeField]
    Transform actor;

    [SerializeField]
    AudioClip cutsceneMusic;

    [SerializeField]
    AudioSource musicAudioSource;

    GameObject player;

    //desired camera to be active
    CutsceneCamera cutsceneControl;

    //other camera types which may be active
    CameraFollow camFollow;
    CameraTopDown camTopDown;
    CameraPlatformer camPlatformer;

    Collider sphereCollider;

    bool play = false;

    [SerializeField]
    float duration = 10.0f;
    // Start is called before the first frame update
    void Start()
    {
        sphereCollider = GetComponent<Collider>();
        cutsceneControl = cam.GetComponent<CutsceneCamera>();
        camFollow = cam.GetComponent<CameraFollow>();
        camTopDown = cam.GetComponent<CameraTopDown>();
        camPlatformer = cam.GetComponent<CameraPlatformer>();
    }

    // Update is called once per frame
    void Update()
    {
        //unfreeze player movement and disable cutscene camera
        if (play)
        {
            duration -= Time.deltaTime;
            if (duration <= 0)
            {
                if (cam)
                {
                    cutsceneControl.enabled = false;
                    camFollow.enabled = true;
                }
                if (player)
                {
                    player.GetComponent<PlayerController>().lockHorizontal = false;
                    player.GetComponent<PlayerController>().lockVertical = false;
                }
                sphereCollider.enabled = false;
                play = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //freeze player and enable cutscene camera while diabling all others
        if (other.CompareTag("Player"))
        {
            play = true;
            //play specific cutscene music
            musicAudioSource.clip = cutsceneMusic;
            musicAudioSource.Play();
            player = other.gameObject;
            player.GetComponent<PlayerController>().lockHorizontal = true;
            player.GetComponent<PlayerController>().lockVertical = true;
            camFollow.enabled = false;
            camTopDown.enabled = false;
            camPlatformer.enabled = false;
            cutsceneControl.enabled = true;
            cutsceneControl.setCutscene(cameraPosition, actor);
        }
    }
}
