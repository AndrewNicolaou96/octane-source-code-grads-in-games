﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractWithRobot : Interactable
{
    //the robots attached gun
    private Turret turret;
    //the players gun
    private GunLogic gun;
    private PlayerController playerController;
    //possible cameras
    private CameraPlatformer camPlatform;
    private CameraFollow camFollow;
    private CameraTopDown camTopDown;
 
    private Robot robot;

    [SerializeField]
    Transform seat;


    [SerializeField]
    GameObject gunGO;

    [SerializeField]
    GameObject m_camera;

    // Start is called before the first frame update
    void Start()
    {
        turret = GetComponent<Turret>();
        gun = gunGO.GetComponent<GunLogic>();
        playerController = player.gameObject.GetComponent<PlayerController>();
        robot = GetComponent<Robot>();
        camPlatform = m_camera.GetComponent<CameraPlatformer>();
        camFollow = m_camera.GetComponent<CameraFollow>();
        camTopDown = m_camera.GetComponent<CameraTopDown>();
    }

    // Update is called once per frame
    void Update()
    {
        base.Update();
    }

    
    public override void interact()
    {
        camFollow.target = transform; //switch camera to focus on robot instead of player
        //disable player gun
        gun.enabled = false;
        turret.enabled = true;
        robot.enabled = true;
        //make the player sit in the robot
        player.position = seat.position;
        //disable player movement and set the vehicle to the robot
        playerController.lockHeight = true;
        playerController.setVehicle(gameObject);
        playerController.lockHorizontal = true;
        playerController.lockVertical = true;
        base.interact();
    }

    //revert interaction()
    public override void leaveInteraction()
    {
        if(camFollow != null)
        {
            camFollow.target = player;
        }
        if (gun != null)
        {
            gun.enabled = true;
        }
        if (turret != null)
        {
            turret.enabled = false;
        }
        if (playerController != null)
        {
            playerController.lockHeight = false;
            playerController.setVehicle(null);
            playerController.lockHorizontal = false;
            playerController.lockVertical = false;
        }
        if(robot != null)
        {
            robot.enabled = false;
        }
        base.leaveInteraction();
    }

    private void OnDisable()
    {
        leaveInteraction();
    }
}
