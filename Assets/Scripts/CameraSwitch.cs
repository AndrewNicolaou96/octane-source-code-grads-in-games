﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{
    //camera types
    private CameraPlatformer camPlatform;
    private CameraFollow camFollow;
    private CameraTopDown camTopDown;
    private PlayerController playerController;

    //camera to switch to
    [SerializeField]
    bool m_PlatformCam;
    [SerializeField]
    bool m_FollowCam;
    [SerializeField]
    bool m_TopDownCam;

    [SerializeField]
    GameObject m_player;

    [SerializeField]
    GameObject m_camera;

    // Start is called before the first frame update
    void Start()
    {
        camPlatform = m_camera.GetComponent<CameraPlatformer>();
        camFollow = m_camera.GetComponent<CameraFollow>();
        camTopDown = m_camera.GetComponent<CameraTopDown>();
        playerController = m_player.GetComponent<PlayerController>();
    }

    //on contact switch to desired camera - focus on player
    void OnTriggerEnter(Collider other)
    {
        if (m_FollowCam)
        {
            if (other.tag == "Player")
            {
                camFollow.enabled = true;
                camPlatform.enabled = false;
                camTopDown.enabled = false;
                playerController.lockVertical = false;
                playerController.lockHorizontal = false;
            }
        }
        if (m_PlatformCam)
        {
            if (other.tag == "Player")
            {
                camFollow.enabled = false;
                camPlatform.enabled = true;
                camTopDown.enabled = false;
                playerController.lockVertical = true;
                playerController.lockHorizontal = false;

            }
        }
        if (m_TopDownCam)
        {
            if (other.tag == "Player")
            {
                camFollow.enabled = false;
                camPlatform.enabled = false;
                camTopDown.enabled = true;
                playerController.lockVertical =   true;
                playerController.lockHorizontal = true;

            }
        }
    }
}
