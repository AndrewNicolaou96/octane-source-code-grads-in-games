﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{

    public void loadMenu()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(0);
    }

    public void loadCampaign()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(1);
    }

    public void loadWaves()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(2);
    }

    public void quitGame()
    {
        Application.Quit();
    }
}
