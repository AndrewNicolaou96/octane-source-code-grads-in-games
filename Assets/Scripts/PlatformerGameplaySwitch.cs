﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformerGameplaySwitch : MonoBehaviour
{
    private bool triggered;

    //cameras to swap
    private CameraPlatformer camPlatform;
    private CameraFollow camFollow; 
    private PlayerController playerController;

    [SerializeField]
    GameObject m_player;
    // The gravity strength
    [SerializeField]
    GameObject m_camera;

    // Start is called before the first frame update
    void Start()
    {
        camPlatform = m_camera.GetComponent<CameraPlatformer>();
        camFollow = m_camera.GetComponent<CameraFollow>();
        playerController = m_player.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //enable platform camera and disable follow camera
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (triggered)
            {
                camFollow.enabled = true;
                camPlatform.enabled = false;
                playerController.lockVertical = false;
                triggered = false;
            }
            else
            {
                camFollow.enabled = false;
                camPlatform.enabled = true;
                playerController.lockVertical = true;
                triggered = true;
            }
        }
    }

}
