﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    [SerializeField]
    GameObject healthBarGO;
    HealthBar healthBar;
    Health health;

    [SerializeField]
    Transform player;

    bool isVisible;

    // Start is called before the first frame update
    void Start()
    {
        healthBar = healthBarGO.GetComponentInChildren<HealthBar>();
        health = GetComponent<Health>();
        //health bar inactive by default
        healthBarGO.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(player.position, transform.position);
        // don't run when too far from player
        if (distance > 150f)
        {
            return;
        }
        if (distance < 100.0f)
        {
            healthBarGO.SetActive(true);
            healthBar.setHealthBar(gameObject, health);
        }
        else
        {
            if (healthBar.getEnemy() == gameObject)
            {
                healthBarGO.SetActive(false);
            }
        }
    }

    //disable ui when healthbar is 0
    private void OnDisable()
    {
        if (healthBar.getEnemy() == gameObject)
        {
            healthBarGO.SetActive(false);
        }
    }

}
