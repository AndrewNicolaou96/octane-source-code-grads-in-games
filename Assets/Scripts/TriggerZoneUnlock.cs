﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//one time use script to unlock zone with non-respawn enemies
public class TriggerZoneUnlock : MonoBehaviour
{
    [SerializeField]
    GameObject zone;
    MeshRenderer zoneMesh;
    Collider zoneBoxCollider;

    private NpcSpawner npcSpawn;

    // Start is called before the first frame update
    void Start()
    {
        npcSpawn = GetComponent<NpcSpawner>();
        zoneMesh = zone.GetComponent<MeshRenderer>();
        zoneBoxCollider = zone.GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        if(npcSpawn.getNpcCount() <= 0)
        {
            zoneMesh.enabled = false;
            zoneBoxCollider.enabled = false;
            enabled = false;
        }
    }
}
