﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    //spawn position
    private Transform originalTransform;

    [SerializeField]
    Camera camera;

    [SerializeField]
    Transform player;

    //where shot will be instantiated
    [SerializeField]
    Transform m_turretFirePoint;

    //the bullets
    [SerializeField]
    GameObject m_turretFirePrefab;

    [SerializeField]
    GameObject explosion;

    //time left until next shot
    [SerializeField]
    float m_ShotCooldown = 0.05f;

    //time between each shot
    float permanentShotCooldown;

    private Health health;

    bool m_CanShoot = true;

    // SFX
    [SerializeField]
    AudioClip m_BulletShot;

    // The AudioSource to play Sounds for this object
    AudioSource m_AudioSource;

    PlayerController playerController;

    //application closing (memory leak protection)
    bool isQuitting;

    // Use this for initialization
    void Start()
    {
        //originalTransform = transform;
        m_AudioSource = GetComponent<AudioSource>();
        health = GetComponent<Health>();
        playerController = player.gameObject.GetComponent<PlayerController>();
        permanentShotCooldown = m_ShotCooldown;
    }

    // Update is called once per frame
    void Update()
    {
        //explode if destroyed
        if (health.getValue() <= 0)
        {
            playerController.AddForce(new Vector3(0.0f, 20.0f, 0.0f));
            Instantiate(explosion, transform.position, transform.rotation);
            gameObject.SetActive(false);
        }
        //mimic player rotation
        Quaternion desiredRotation = Quaternion.Euler(transform.rotation.eulerAngles.x, player.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
        transform.rotation = desiredRotation;
        if (!m_CanShoot)
        {
            m_ShotCooldown -= Time.deltaTime;
            if (m_ShotCooldown < 0.0f)
            {
                m_CanShoot = true;
                m_ShotCooldown = permanentShotCooldown;
            }
        }
        if (m_CanShoot)
        {
            if (Input.GetButton("Fire1"))
            {
                Fire();
                m_CanShoot = false;
            }
        }
    }

    void Fire()
    {

        if (camera != null)
        {
            //Ray ray = new Ray(camera.transform.position, camera.transform.forward);
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100))
            {
                m_turretFirePoint.LookAt(hit.point);
            }
            Instantiate(m_turretFirePrefab, m_turretFirePoint.position, m_turretFirePoint.rotation * m_turretFirePrefab.transform.rotation);
        }
        else
        {
            Instantiate(m_turretFirePrefab, m_turretFirePoint.position, transform.rotation * m_turretFirePrefab.transform.rotation);
        }
        // Play Sound effect
        if (m_AudioSource && m_BulletShot)
        {
            m_AudioSource.PlayOneShot(m_BulletShot);
        }

    }

    private void OnDisable()
    {
        if (!isQuitting)
        {
            if (health.getValue() <= 0)
            {
                playerController.AddForce(new Vector3(0.0f, 5.0f, 0.0f));
                Instantiate(explosion, transform.position, transform.rotation);
            }
        }
    }

    private void OnApplicationQuit()
    {
        isQuitting = true;
    }

}
