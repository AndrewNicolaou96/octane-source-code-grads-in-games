﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillOnContact : MonoBehaviour
{
    //kill player and destroy enemies on contact
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<Health>().setValue(0);
        }
        if (other.CompareTag("Enemy"))
        {
            Destroy(other.gameObject);
        }
    }
}
