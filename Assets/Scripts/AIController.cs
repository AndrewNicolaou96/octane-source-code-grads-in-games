﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    // --------------------------------------------------------------
    //spawn transform
    private Transform originalTransform;
    // The character's running speed
    [SerializeField]
    float m_MovementSpeed = 4.0f;

    // The gravity strength
    [SerializeField]
    float m_Gravity = 60.0f;

    // The maximum speed the character can fall
    [SerializeField]
    float m_MaxFallSpeed = 20.0f;

    [SerializeField]
    float m_AggroRange = 30.0f;

    [SerializeField]
    int damage = 10;

    [SerializeField]
    GameObject explosion;

    //certain enemies should not respawn
    [SerializeField]
    bool dontRespawn;

    [SerializeField]
    GameObject bloodPrefab;

    bool isQuitting = false;
    // --------------------------------------------------------------

    // The charactercontroller of the player
    CharacterController m_CharacterController;

    // The current movement direction in x & z.
    Vector3 m_MovementDirection = Vector3.zero;

    // The current vertical / falling speed
    float m_VerticalSpeed = 0.0f;

    // The current movement offset
    Vector3 m_CurrentMovementOffset = Vector3.zero;

    PlayerController m_PlayerController;
    Health m_PlayerHealth;
    Transform m_PlayerTransform;

    //the players gun
    GunLogic gun;

    AudioSource audiosource;

    // --------------------------------------------------------------

    void Awake()
    {
        m_CharacterController = GetComponent<CharacterController>();
    }

    // Use this for initialization
    void Start()
    {
        audiosource = GetComponent<AudioSource>();
        // Get Player information
        gun = GetComponent<GunLogic>();
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if(player)
        {
            m_PlayerController = player.GetComponent<PlayerController>();
            m_PlayerHealth = player.GetComponent<Health>();
            m_PlayerTransform = player.transform;
        }
        
    }

    void ApplyGravity()
    {
        // Apply gravity
        m_VerticalSpeed -= m_Gravity * Time.deltaTime;

        // Make sure we don't fall any faster than m_MaxFallSpeed.
        m_VerticalSpeed = Mathf.Max(m_VerticalSpeed, -m_MaxFallSpeed);
        m_VerticalSpeed = Mathf.Min(m_VerticalSpeed, m_MaxFallSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        float distance = 0;
        if (m_PlayerController.getVehicle() == null)
        {
            distance = Vector3.Distance(m_PlayerTransform.position, transform.position);
        }
        else
        {
            distance = Vector3.Distance(m_PlayerController.getVehicle().transform.position, transform.position);
        }
        // don't run when too far from player
        if (distance > 150)
        {
            return;
        }

        // Aggro range
        if (distance < m_AggroRange)
        {
            m_MovementDirection = m_PlayerTransform.position - transform.position;
            m_MovementDirection.Normalize();
        }
        else
        {
            m_MovementDirection = Vector3.zero;
        }

        // Attack range
        if(distance < 2.0f)
        {
            // Knock back player
            if (m_PlayerController.getVehicle() == null)
            {
                float force = 5.0f;
                if (explosion)
                {
                    force = 20f;
                    Instantiate(explosion, transform.position, explosion.transform.rotation);
                    Destroy(gameObject);
                }
                m_PlayerController.AddForce((m_MovementDirection + new Vector3(0, 2, 0)) * force);
                m_PlayerHealth.DoDamage(damage);
            }
            else
            {
                m_PlayerController.getVehicleHealth().DoDamage(damage);
                Destroy(gameObject);
            }
            
        }

        // Update jumping input and apply gravity
        ApplyGravity();

        // Calculate actual motion
        m_CurrentMovementOffset = (m_MovementDirection * m_MovementSpeed + new Vector3(0, m_VerticalSpeed, 0)) * Time.deltaTime;

        // Move character
        m_CharacterController.Move(m_CurrentMovementOffset);

        // Rotate the character in movement direction
        if(m_MovementDirection != Vector3.zero)
        {
            RotateCharacter(m_MovementDirection);
        }
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    void RotateCharacter(Vector3 movementDirection)
    {
        Quaternion lookRotation = Quaternion.LookRotation(movementDirection);
        if (transform.rotation != lookRotation)
        {
            transform.rotation = lookRotation;
        }
    }

    private void OnApplicationQuit()
    {
        isQuitting = true;
    }

    private void OnDisable()
    {
        if (!isQuitting)
        {
            //blood effects
            if (audiosource)
            {
                audiosource.Play();
            }
            Instantiate(bloodPrefab, transform.position, transform.rotation);
        }
        m_PlayerHealth.setValue(Mathf.Clamp(m_PlayerHealth.getValue() + 3, 0, m_PlayerHealth.getMax()));
        if (dontRespawn)
        {
            Destroy(gameObject);
        }
    }

    public void Die()
    {

    }


    

}
