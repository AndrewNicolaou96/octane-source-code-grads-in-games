﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    // --------------------------------------------------------------

    // The character's running speed
    [SerializeField]
    float m_RunSpeed = 5.0f;
    // The maximum speed the character can fall
    [SerializeField]
    float m_MaxFallSpeed = 20.0f;
    [SerializeField]
    GameObject pauseMenu;

    // --------------------------------------------------------------

    //-----------------PRIVATE--------------------//

    // The charactercontroller of the player
    CharacterController m_CharacterController;
    // The current vertical / falling speed
    float m_VerticalSpeed = 0.0f;
    // The starting position of the player
    Vector3 m_SpawningPosition = Vector3.zero;
    // Whether the player is alive or not
    bool m_IsAlive = true;
    // The time it takes to respawn
    const float MAX_RESPAWN_TIME = 1.0f;
    float m_RespawnTime = MAX_RESPAWN_TIME;
    // The force added to the player (used for knockbacks)
    Vector3 m_Force = Vector3.zero;
    Health vehicleHealth = null;
    //gravity at normal timescale
    private float originalGravity = 60f;
    GameObject vehicle = null;



    //-----------------PUBLIC--------------------//
    // The gravity strength
    public float m_Gravity = 60.0f;
    //gravity at slow timescale 
    public float m_GravitySlowMo;
    // The current movement offset
    public Vector3 m_CurrentMovementOffset = Vector3.zero;
    // The current movement direction in x & z.
    public Vector3 m_MovementDirection = Vector3.zero;
    // The current movement speed
    public float m_MovementSpeed = 0.0f;
    //lock movement in xyz axes
    public bool lockHeight = false;
    public bool lockHorizontal = false;
    public bool lockVertical = false;
    //slow motion is enabled
    public bool slowMo = false;
    public bool isPaused;
    // The character's jump height
    public float m_JumpHeight = 4.0f;


    // --------------------------------------------------------------

    void Awake()
    {
        m_CharacterController = GetComponent<CharacterController>();
    }

    // Use this for initialization
    void Start()
    {
        m_GravitySlowMo = m_Gravity * 10;
        m_SpawningPosition = transform.position;
    }

    void Jump()
    {
        m_VerticalSpeed = Mathf.Sqrt(m_JumpHeight * m_Gravity);
    }

    void ApplyGravity()
    {
        if (!lockHeight)
        {
            // Apply gravity
            m_VerticalSpeed -= m_Gravity * Time.deltaTime;

            // Make sure we don't fall any faster than m_MaxFallSpeed.
            m_VerticalSpeed = Mathf.Max(m_VerticalSpeed, -m_MaxFallSpeed);
            m_VerticalSpeed = Mathf.Min(m_VerticalSpeed, m_MaxFallSpeed);
        }
    }

    void UpdateMovementState()
    {
        float horizontalInput = 0f;
        float verticalInput = 0f;
        // Get Player's movement input and determine direction and set run speed
        if (!lockHorizontal)
        {
            horizontalInput = Input.GetAxisRaw("Horizontal_P1");
        }
        if (!lockVertical)
        {
            verticalInput = Input.GetAxisRaw("Vertical_P1");
        }

        m_MovementDirection = new Vector3(horizontalInput, 0, verticalInput);
        m_MovementSpeed = m_RunSpeed;
    }

    void UpdateJumpState()
    {
        // Character can jump when standing on the ground
        if (Input.GetButtonDown("Jump_P1") && m_CharacterController.isGrounded)
        {
            Jump();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //ESC pauses game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
            {
                pauseMenu.SetActive(false);
                isPaused = false;
            }
            else
            {
                pauseMenu.SetActive(true);
                isPaused = true;
            }
        }

        if (!isPaused)
        {
            //increase gravity effect wehn in slowmo
            if (slowMo)
            {
                m_Gravity = m_GravitySlowMo;
            }
            else
            {
                m_Gravity = originalGravity;
            }


            // If the player is dead update the respawn timer and exit update loop
            if (!m_IsAlive)
            {
                //UpdateRespawnTime();
                return;
            }

            // Update movement input
            UpdateMovementState();

            // Update jumping input and apply gravity
            UpdateJumpState();
            ApplyGravity();

            // Calculate actual motion
            m_CurrentMovementOffset = (m_MovementDirection * m_MovementSpeed + m_Force + new Vector3(0, m_VerticalSpeed, 0)) * Time.deltaTime / Time.timeScale;

            m_Force *= 0.95f;

            // Move character
            m_CharacterController.Move(m_CurrentMovementOffset);

            // Rotate the character towards the mouse cursor
            RotateCharacterTowardsMouseCursor();
        }
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    void RotateCharacter(Vector3 movementDirection)
    {
        Quaternion lookRotation = Quaternion.LookRotation(movementDirection);
        if (transform.rotation != lookRotation)
        {
            transform.rotation = lookRotation;
        }
    }

    void RotateCharacterTowardsMouseCursor()
    {
        Vector3 mousePosInScreenSpace = Input.mousePosition;
        Vector3 playerPosInScreenSpace = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 directionInScreenSpace = mousePosInScreenSpace - playerPosInScreenSpace;

        float angle = Mathf.Atan2(directionInScreenSpace.y, directionInScreenSpace.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(-angle + 90.0f, Vector3.up);
    }

    public void Die()
    {
        m_IsAlive = false;
        m_RespawnTime = MAX_RESPAWN_TIME;
    }

    public void AddForce(Vector3 force)
    {
        m_Force += force;
    }

    //used to pair player with a vehicle
    public void setVehicle(GameObject v)
    {
        vehicle = v;
        if (vehicle != null)
        {
            vehicleHealth = vehicle.GetComponent<Health>();
        }
    }

    public void removeVehicle()
    {
        vehicle = null;
        vehicleHealth = null;
    }
    
    public GameObject getVehicle()
    {
        return vehicle;
    }

    public Health getVehicleHealth()
    {
        return vehicleHealth;
    }
}
