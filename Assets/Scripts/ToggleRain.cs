﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleRain : MonoBehaviour
{
    [SerializeField]
    GameObject rain;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            rain.SetActive(false);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            rain.SetActive(true);
        }
    }
}
