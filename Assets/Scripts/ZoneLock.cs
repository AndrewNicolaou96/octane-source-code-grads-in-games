﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneLock : MonoBehaviour
{
    //enemies that are keeping the zone locked
    [SerializeField]
    Transform enemies;

    GameObject player;

    private MeshRenderer mesh;
    private Collider collider;
    bool isUnlocked = false;
    // Start is called before the first frame update
    void Start()
    {
        player  = GameObject.FindGameObjectWithTag("Player");
        mesh = GetComponent<MeshRenderer>();
        collider = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(player.transform.position, transform.position);
        //dont run when out of range
        if (distance < 100f)
        {
            //don't run once zone is open
            if (isUnlocked)
            {
                return;
            }
            //check is all children are inactive (enemies killed)
            int count = 0; //active enemies
            foreach (Transform enemy in enemies)
            {
                if (enemy.gameObject.activeSelf)
                {
                    count++;
                }
            }
            //if no enemies left then unlock zone
            if (count == 0)
            {
                mesh.enabled = false;
                collider.enabled = false;
                isUnlocked = true;
            }
            else
            {
                mesh.enabled = true;
                collider.enabled = true;
            }
        }
    }
}
