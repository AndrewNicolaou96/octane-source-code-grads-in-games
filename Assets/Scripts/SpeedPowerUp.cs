﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPowerUp : MonoBehaviour
{
    [SerializeField]
    GameObject powerUpManager;

    SlowMotion slowMotionScript;

    private void Start()
    {
        slowMotionScript = powerUpManager.GetComponent<SlowMotion>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!slowMotionScript.slowMo)
            {
                slowMotionScript.slowMo = true;
                gameObject.SetActive(false);
            }
        }
    }



}
