﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//just a thank you message
public class EndGame : MonoBehaviour
{
    [SerializeField]
    GameObject thankYouMessage;

    private void OnDisable()
    {
        Invoke("thanks", 3.0f);
    }

    void thanks()
    {
        thankYouMessage.SetActive(true);
    }
}
