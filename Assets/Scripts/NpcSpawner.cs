﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject m_NpcPrefab;
    [SerializeField]
    float m_PermanentSpawnCooldown;

    private float m_SpawnCooldown;
    [SerializeField]
    int m_NpcCount;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //spawn npc while there still is a count of them
        m_SpawnCooldown -= Time.deltaTime;
        if (m_NpcCount > 0 && m_SpawnCooldown <= 0)
        {
            spawn();
            m_NpcCount--;
            m_SpawnCooldown = m_PermanentSpawnCooldown;
        }
    }

    void spawn()
    {
        Instantiate(m_NpcPrefab, transform.position, transform.rotation);
    }

    public int getNpcCount()
    {
        return m_NpcCount;
    }

    public void setNpcCount(int count)
    {
        this.m_NpcCount = count;
    }

    public void setCoolDown(float time)
    {
        this.m_PermanentSpawnCooldown = time;
    }
}
