﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonAI : MonoBehaviour
{
    [SerializeField]
    float aggression = 1.0f; //cool down divider

    [SerializeField]
    Transform player;

    [SerializeField]
    GameObject fireBallAttsck;

    [SerializeField]
    ParticleSystem fireBreath;

    //time between fire breath move
    [SerializeField]
    float permanentBreathCoolDown;
    private float breathCoolDown;

    //time between neck swing move
    [SerializeField]
    float permanentNeckSwingCoolDown;
    private float neckSwingCoolDown;

    //time between fireball move
    [SerializeField]
    float permanentFireBallCoolDown;
    private float fireBallhCoolDown;

    //Time taken to perform an attack
    [SerializeField]
    float permanentAttackTime = 3f;
    float attackTime;

    [SerializeField]
    float meleeRange = 30;

    [SerializeField]
    float fireBallRange = 50;

    [SerializeField]
    float maxRange = 100;

    [SerializeField]
    GameObject deathEffect;

    [SerializeField]
    Transform deathEffectOrigin;

    public bool isAttacking = false;

    private Health health; 
    private int halfHealth;
    private int quaterHealth;
    private AudioSource audioSource;
    private bool isQuitting;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        breathCoolDown = permanentBreathCoolDown / aggression;
        neckSwingCoolDown = permanentNeckSwingCoolDown / aggression;
        attackTime = permanentAttackTime / aggression;
        health = GetComponent<Health>();
        halfHealth = health.getValue() / 2;
        quaterHealth = halfHealth / 2;
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(player.position, transform.position);

        // don't run when too far from player
        if (distance > 150)
        {
            return;
        }
        //increase aggression as health drops
        if (health.getValue() < halfHealth)
        {
            aggression = 1.5f;
            if(health.getValue() < quaterHealth)
            {
                aggression = 3;
            }
        }

        //reset animations
        anim.SetBool("swingNeck", false);
        anim.SetBool("breatheFire", false);
        if (attackTime <= 0)
        {
            isAttacking = false;
        }

        int rng = Random.Range(1, 11); //RANDOM NUMBER GENERATOR    

        //only attack if player is in range
        if (distance < maxRange)
        {
            //test to see if melee attacks are possible
            if (distance < fireBallRange)
            {
                if (!isAttacking)
                {
                    //rng to decide attack
                    if (rng > 9 && breathCoolDown <= 0)
                    {
                        breatheFire();
                        breathCoolDown = permanentBreathCoolDown / aggression;
                    }
                    else if (rng > 7 && neckSwingCoolDown <= 0 && Vector3.Distance(player.position, transform.position) < meleeRange)
                    {
                        neckSwing();
                        neckSwingCoolDown = permanentNeckSwingCoolDown / aggression;
                    }
                    else if (rng > 5 && fireBallhCoolDown <= 0)
                    {
                        fireBall();
                        fireBallhCoolDown = permanentFireBallCoolDown / aggression;
                    }
                }
            }
            else if (fireBallhCoolDown <= 0) //use fireballs if player too far
            {
                fireBall();
                fireBallhCoolDown = permanentFireBallCoolDown / aggression;
            }
        }
        neckSwingCoolDown -= Time.deltaTime;
        fireBallhCoolDown -= Time.deltaTime;
        breathCoolDown -= Time.deltaTime;
        attackTime -= Time.deltaTime;
    }

    void breatheFire()
    {
        anim.SetBool("breatheFire", true);
        //play sound
        isAttacking = true;
        attackTime = permanentAttackTime / aggression;
    }

    void neckSwing()
    {
        anim.SetBool("swingNeck", true);
        isAttacking = true;
        attackTime = permanentAttackTime / aggression;

    }

    void fireBall()
    {
        isAttacking = true;
        attackTime = permanentAttackTime / aggression;
        Instantiate(fireBallAttsck, player.position, fireBallAttsck.transform.rotation);
    }

    void playFireParticles()
    {
        fireBreath.Play();
        audioSource.Play();
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, meleeRange); //visulaises the range of melee attacks

    }

    private void OnDisable()
    {
        if (!isQuitting)
        {
            //death effect
            Instantiate(deathEffect, deathEffectOrigin.position, deathEffectOrigin.rotation);
        }
    }


    private void OnApplicationQuit()
    {
        isQuitting = true;
    }

}
