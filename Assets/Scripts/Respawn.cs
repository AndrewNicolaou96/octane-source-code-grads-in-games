﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    public static Respawn instance;

    GameObject player;

    Health playerHealth;

    Transform lastCheckPoint;

    private AIController[] enemies;
    private ShooterAIController[] shooterEnemies;
    private Interactable[] interactables;
    private Health[] health;
    private SpeedPowerUp[] speedPU;
    private DamagePowerUp[] damagePU;
    private ExplosivePowerUp[] explosivePU;
    private GameObject musicGO;
    private AudioSource music;

    [SerializeField]
    AudioClip defaultMusicClip;

    // Start is called before the first frame update
    void Start()
    {
        //singletom
        if (instance != null)
        {
            Debug.Log("Only 1 respawn manager allowed in scene");
        }
        instance = this;
        //find music and player
        player = GameObject.FindGameObjectWithTag("Player");
        musicGO = GameObject.FindGameObjectWithTag("Music");
        music = musicGO.GetComponent<AudioSource>();
        //put alll respawnable types in list
        enemies = Resources.FindObjectsOfTypeAll<AIController>();
        shooterEnemies = Resources.FindObjectsOfTypeAll<ShooterAIController>();
        interactables = Resources.FindObjectsOfTypeAll<Interactable>();
        health = Resources.FindObjectsOfTypeAll<Health>();
        speedPU = Resources.FindObjectsOfTypeAll<SpeedPowerUp>();
        damagePU = Resources.FindObjectsOfTypeAll<DamagePowerUp>();
        explosivePU = Resources.FindObjectsOfTypeAll<ExplosivePowerUp>();
        playerHealth = player.GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        if(playerHealth.getValue() <= 0)
        {
            respawn();
        }
    }

    void respawn()
    {
        Camera.main.transform.position = player.transform.position;
        //reset music
        music.clip = defaultMusicClip;
        music.Play();
        //respawn player
        playerHealth.setValue(100);
        player.transform.position = lastCheckPoint.position;
        //respawn enemies
        foreach(AIController enemy in enemies)
        {
            if (enemy)
            {
                if (!enemy.isActiveAndEnabled)
                {
                    enemy.gameObject.SetActive(true);
                }
            }
        }
        //melee and ranged enemies are classed as seperate type
        foreach(ShooterAIController enemy in shooterEnemies)
        {
            if (enemy)
            {
                if (!enemy.isActiveAndEnabled)
                {
                    enemy.gameObject.SetActive(true);
                }
            }
        }
        //respawn interactables
        foreach(Interactable i in interactables){
            if (!i.isActiveAndEnabled && i != null)
            {
                i.gameObject.SetActive(true);
            }
        }
        //reset health of everything
        foreach(Health h  in health)
        {
            h.setValue(h.getMax());
        }
        //respawn powerups
        foreach (SpeedPowerUp s in speedPU)
        {
            s.gameObject.SetActive(true);
        }
        foreach (DamagePowerUp d in damagePU)
        {
            d.gameObject.SetActive(true);
        }
        foreach (ExplosivePowerUp e in explosivePU)
        {
            e.gameObject.SetActive(true);
        }
    }

    public void setLastCheckpoint(Transform checkpoint)
    {
        this.lastCheckPoint = checkpoint;
    }
}
