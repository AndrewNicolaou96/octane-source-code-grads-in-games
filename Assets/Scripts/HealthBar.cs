﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthBar : MonoBehaviour
{

    GameObject enemy;
    Health health;
    Image healthBarImage;


    // Start is called before the first frame update
    void Start()
    {
        healthBarImage = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        //display partially filled healthbar
        if (enemy)
        {
            healthBarImage.fillAmount = (float)health.getValue() / (float)health.getMax();
        }
    }

    public void setHealthBar(GameObject enemy, Health health)
    {
        if (enemy)
        {
            this.enemy = enemy;
            this.health = health;
        }
    }

    public GameObject getEnemy()
    {
        return enemy;
    }
}
