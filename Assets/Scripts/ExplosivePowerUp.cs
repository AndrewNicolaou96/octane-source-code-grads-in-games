﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosivePowerUp : MonoBehaviour
{
    [SerializeField]
    GameObject powerUpManager;

    ExplosiveBullets explosiveBulletsScript;

    private void Start()
    {
        explosiveBulletsScript = powerUpManager.GetComponent<ExplosiveBullets>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            explosiveBulletsScript.explosive = true;
            gameObject.SetActive(false);
        }
    }
}
