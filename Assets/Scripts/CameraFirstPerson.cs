﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//DEPRECATED
public class CameraFirstPerson : MonoBehaviour
{
    // The Camera Target
    [SerializeField]
    Transform m_PlayerTransform;

    [SerializeField]
    float m_CameraDistanceZ = 15.0f;

    [SerializeField]
    float m_CameraDistanceY = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = m_PlayerTransform.position;
        transform.rotation = m_PlayerTransform.rotation;
    }
}
