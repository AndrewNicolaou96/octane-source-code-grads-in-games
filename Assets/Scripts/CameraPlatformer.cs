﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlatformer : MonoBehaviour
{
    private Vector3 anchor;
    private bool locked;
    private Camera camera;

    public Transform target;

    // The Camera Target
    [SerializeField]
    Transform m_PlayerTransform;

    [SerializeField]
    float m_CameraDistanceZ = 15.0f;

    [SerializeField]
    float m_CameraDistanceY = 2.0f;

    [SerializeField]
    float m_moveSpeed = 10.0f;

    [SerializeField]
    float m_fov = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponent<Camera>();
        target = m_PlayerTransform;
    }

    // Update is called once per frame
    void Update()
    {
        //move  camera to anchor relative to target
        anchor = new Vector3(m_PlayerTransform.position.x, m_PlayerTransform.position.y + m_CameraDistanceY, m_PlayerTransform.position.z - m_CameraDistanceZ);
        //lock camera position once it's reach the anchor
        if (!locked)
        {
            float distanceToMove = m_moveSpeed * Time.deltaTime;
            Vector3 direction = anchor - transform.position; 
            if (Vector3.Distance(anchor, transform.position) > 0.5f)
            {
                transform.Translate(direction.normalized * distanceToMove, Space.World);
            }
            else
            {
                locked = true;
            }
        }
        else
        {
            transform.position = anchor;
        }
        transform.LookAt(target);
    }

    void OnEnable()
    {
        locked = false;
    }


}
