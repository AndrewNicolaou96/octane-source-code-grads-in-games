﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePowerUp : MonoBehaviour
{
    [SerializeField]
    GameObject powerUpManager;
    IncreaseDamage increaseDamageScript;

    private void Start()
    {
        increaseDamageScript = powerUpManager.GetComponent<IncreaseDamage>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            increaseDamageScript.increasedDamage = true;
            gameObject.SetActive(false);
        }
    }
}
