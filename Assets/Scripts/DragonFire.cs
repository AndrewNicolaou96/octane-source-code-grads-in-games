﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonFire : MonoBehaviour
{
    [SerializeField]
    int m_Damage = 40;

    private void OnParticleCollision(GameObject other)
    {
        Health health = other.GetComponent<Health>();
        if (health && other.CompareTag("Player"))
        {
            health.DoDamage(m_Damage);
        }
    }
}
