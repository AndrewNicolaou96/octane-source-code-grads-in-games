﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBulletLogic : BulletLogic
{
    // The Explosion ParticleEmitter Prefab
    [SerializeField]
    GameObject m_ExplosionPE;

    [SerializeField]
    float explosionRadius = 8;

    protected override void Explode()
    {
        if (m_ExplosionPE)
        {
            Instantiate(m_ExplosionPE, transform.position, transform.rotation);
        }
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach(Collider c in colliders)
        {
            if (c.CompareTag("Enemy")){
                c.gameObject.GetComponent<Health>().DoDamage(m_Damage);
            }
        }
    }
}
