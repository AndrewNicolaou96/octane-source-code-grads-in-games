﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTopDown : MonoBehaviour
{
    private Vector3 anchor;
    private bool locked;
    private Camera camera;

    // The Camera Target
    [SerializeField]
    Transform m_PlayerTransform;

    // The Z Distance from the Camera Target
    [SerializeField]
    float m_CameraDistanceY = 15.0f;

    [SerializeField]
    float m_moveSpeed = 10.0f;

    [SerializeField]
    float m_fov = 60.0f;
    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        //move camera towards anchor
        anchor = new Vector3(m_PlayerTransform.position.x, m_PlayerTransform.position.y + m_CameraDistanceY, m_PlayerTransform.position.z);
        //stop moving once anchor is reached
        if (!locked)
        {
            float distanceToMove = m_moveSpeed * Time.deltaTime;
            Vector3 direction = anchor - transform.position;
            if (Vector3.Distance(anchor, transform.position) > 0.5f)
            {
                transform.Translate(direction.normalized * distanceToMove, Space.World);
            }
            else
            {
                locked = true;
            }
        }
        else
        {
            transform.position = anchor;
        }
        transform.LookAt(m_PlayerTransform);
    }

    void OnEnable()
    {
        gameObject.GetComponent<Camera>().orthographic = false;
        locked = false;
    }
}
