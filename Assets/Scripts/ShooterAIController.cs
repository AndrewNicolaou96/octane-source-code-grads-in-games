﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterAIController : MonoBehaviour
{
    // --------------------------------------------------------------
    //spawn transform
    private Transform originalTransform;

    // The character's running speed
    [SerializeField]
    float m_MovementSpeed = 4.0f;

    // The gravity strength
    [SerializeField]
    float m_Gravity = 60.0f;

    // The maximum speed the character can fall
    [SerializeField]
    float m_MaxFallSpeed = 20.0f;

    [SerializeField]
    float m_AggroRange = 30.0f;

    [SerializeField]
    GameObject bulletFirePrefab;

    [SerializeField]
    Transform bulletFirePoint;

    //time between shots
    [SerializeField]
    float permanentShotCooldown = 0.5f;
    //time until next shot
    private float shotCoolDown;

    [SerializeField]
    GameObject bloodPrefab;


    //if the application is quitting (for preventing memory leaks)
    bool isQuitting = false;
    // --------------------------------------------------------------

    // The charactercontroller of the player
    CharacterController m_CharacterController;

    // The current movement direction in x & z.
    Vector3 m_MovementDirection = Vector3.zero;

    // The current vertical / falling speed
    float m_VerticalSpeed = 0.0f;

    // The current movement offset
    Vector3 m_CurrentMovementOffset = Vector3.zero;

    // Whether the player is alive or not
    bool m_IsAlive = true;

    PlayerController m_PlayerController;
    Health m_PlayerHealth;
    Transform m_PlayerTransform;

    private AudioSource audioSource;

    // --------------------------------------------------------------

    void Awake()
    {
        m_CharacterController = GetComponent<CharacterController>();
    }

    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        shotCoolDown = permanentShotCooldown;
        // Get Player information
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player)
        {
            m_PlayerController = player.GetComponent<PlayerController>();
            m_PlayerHealth = player.GetComponent<Health>();
            m_PlayerTransform = player.transform;
        }

    }

    void ApplyGravity()
    {
        // Apply gravity
        m_VerticalSpeed -= m_Gravity * Time.deltaTime;

        // Make sure we don't fall any faster than m_MaxFallSpeed.
        m_VerticalSpeed = Mathf.Max(m_VerticalSpeed, -m_MaxFallSpeed);
        m_VerticalSpeed = Mathf.Min(m_VerticalSpeed, m_MaxFallSpeed);
    }

    // Update is called once per frame
    void Update()
    {

        float distance = 0;
        if (m_PlayerController.getVehicle() == null)
        {
            distance = Vector3.Distance(m_PlayerTransform.position, transform.position);
        }
        else
        {
            distance = Vector3.Distance(m_PlayerController.getVehicle().transform.position, transform.position);
        }
        // don't run when too far from player
        if (distance > 150)
        {
            return;
        }

        // Aggro range
        if (distance < m_AggroRange)
        {
            if (shotCoolDown <= 0)
            {
                Instantiate(bulletFirePrefab, bulletFirePoint.position, bulletFirePoint.rotation);
                //Instantiate(bulletFirePrefab, bulletFirePoint.position, bulletFirePoint.rotation);
                shotCoolDown = permanentShotCooldown;
            }
            m_MovementDirection = m_PlayerTransform.position - transform.position;
            //Rotate the character in movement direction
            m_MovementDirection.Normalize();
            if (m_MovementDirection != Vector3.zero)
            {
                RotateCharacter(m_MovementDirection);
            }
        }
        else
        {
            m_MovementDirection = Vector3.zero;
        }

        // keep distance from player 
        if (distance < 10.0f)
        {
            //stop moving
            m_MovementDirection = Vector3.zero;
        }

        // Update jumping input and apply gravity
        ApplyGravity();

        // Calculate actual motion
        m_CurrentMovementOffset = (m_MovementDirection * m_MovementSpeed + new Vector3(0, m_VerticalSpeed, 0)) * Time.deltaTime;

        // Move character
        m_CharacterController.Move(m_CurrentMovementOffset);

        //transform.LookAt(m_PlayerTransform);
        shotCoolDown -= Time.deltaTime;
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    void RotateCharacter(Vector3 movementDirection)
    {
        Quaternion lookRotation = Quaternion.LookRotation(movementDirection);
        if (transform.rotation != lookRotation)
        {
            transform.rotation = lookRotation;
        }
    }

    private void OnApplicationQuit()
    {
        isQuitting = true;
    }

    private void OnDisable ()
    {
        //regen player health to upper limit of 100
        m_PlayerHealth.setValue(Mathf.Clamp(m_PlayerHealth.getValue() + 3, 0, m_PlayerHealth.getMax()));
        if (!isQuitting)
        {
            // Play  blod Sound effect
            if (audioSource)
            {
                audioSource.Play();
            }
            Instantiate(bloodPrefab, transform.position, bloodPrefab.transform.rotation);
        }

    }

    public void Die()
    {
        m_IsAlive = false;
    }

    public void resetTransform()
    {
        //transform.position = originalTransform.position;
        //transform.rotation = originalTransform.rotation;
    }
}
