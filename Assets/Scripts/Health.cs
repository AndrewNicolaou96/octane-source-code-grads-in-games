﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    // The total health of this unit
    [SerializeField]
    int m_Health = 100;

    private int maxHealth;

    private void Awake()
    {
        maxHealth = m_Health;
    }

    public void DoDamage(int damage)
    {
        m_Health -= damage;

        if(m_Health < 0 && !CompareTag("Player") &&!CompareTag("Interactable"))
        {
            gameObject.SetActive(false);
        }
    }

    public bool IsAlive()
    {
        return m_Health > 0;
    }

    public int getValue()
    {
        return m_Health;
    }

    public void setValue(int value)
    {
        this.m_Health = value;
    }

    public int getMax()
    {
        return maxHealth;
    }

    private void OnEnable()
    {
        m_Health = maxHealth;
    }
}
