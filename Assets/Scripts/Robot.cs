﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot : MonoBehaviour
{

    Animator animator;

    [SerializeField]
    float m_RunSpeed = 5.0f;

    [SerializeField]
    Transform player;

    [SerializeField]
    Transform seat;

    PlayerController playerController;


    // The charactercontroller of the player
    CharacterController m_CharacterController;

    // The current movement direction in x & z.
    public Vector3 m_MovementDirection = Vector3.zero;

    // The current movement speed
    public float m_MovementSpeed = 0.0f;

    // The current vertical / falling speed
    float m_VerticalSpeed = 0.0f;

    // The current movement offset
    public Vector3 m_CurrentMovementOffset = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        playerController = player.gameObject.GetComponent<PlayerController>();
        m_CharacterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateMovementState();
        m_CurrentMovementOffset = (m_MovementDirection * m_MovementSpeed + new Vector3(0, m_VerticalSpeed, 0)) * Time.deltaTime;
        m_CharacterController.Move(m_CurrentMovementOffset);

        // Rotate the character towards the mouse cursor
        RotateCharacterTowardsMouseCursor();

        ////check if moving - must be slightly higher than 0
        if (Mathf.Abs(m_CurrentMovementOffset.x) > 0.1f || Mathf.Abs(m_CurrentMovementOffset.z) > 0.1f)
        {
            animator.SetBool("isRunning", true);
        }
        else
        {
            animator.SetBool("isRunning", false);
        }
        //sit player inside robot
        player.position = seat.position;

    }

    //similar controls to unmounted player
    void UpdateMovementState()
    {
        float horizontalInput = 0f;
        float verticalInput = 0f;
        // Get Player's movement input and determine direction and set run speed
        horizontalInput = Input.GetAxisRaw("Horizontal_P1");

        verticalInput = Input.GetAxisRaw("Vertical_P1");

        m_MovementDirection = new Vector3(horizontalInput, 0, verticalInput);
        m_MovementSpeed = m_RunSpeed;
    }

    void RotateCharacter(Vector3 movementDirection)
    {
        Quaternion lookRotation = Quaternion.LookRotation(movementDirection);
        if (transform.rotation != lookRotation)
        {
            transform.rotation = lookRotation;
        }
    }

    void RotateCharacterTowardsMouseCursor()
    {
        Vector3 mousePosInScreenSpace = Input.mousePosition;
        Vector3 playerPosInScreenSpace = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 directionInScreenSpace = mousePosInScreenSpace - playerPosInScreenSpace;

        float angle = Mathf.Atan2(directionInScreenSpace.y, directionInScreenSpace.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(-angle + 90.0f, Vector3.up);
    }

    private void OnDisable()
    {
        animator.SetBool("isRunning", false);
    }

}
