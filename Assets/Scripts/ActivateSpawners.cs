﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateSpawners : MonoBehaviour
{
    [SerializeField]
    Transform[] children;

    //interval between activation of each spawner
    [SerializeField]
    float permanentActivationInterval;

    //time between each spawner activation
    private float activationInterval;

    //current spawner to manipulate
    int index = 1;
    // Start is called before the first frame update
    void Start()
    {
        activationInterval = permanentActivationInterval;
        children = GetComponentsInChildren<Transform>(true);
    }

    // Update is called once per frame
    void Update()
    {
        if(index >= children.Length - 1)
        {
            this.enabled = false;
        }
        //activate spawners one after enother with an interval of n seconds
        activationInterval -= Time.deltaTime;
        if (activationInterval <= 0)
        {
            children[index].gameObject.SetActive(true);
            activationInterval = permanentActivationInterval;
            index++;
        }

    }
}
