﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractWithTurret : Interactable
{
    private Turret turret;
    private GunLogic gun;
    private PlayerController playerController;
    private CameraPlatformer camPlatform;
    private CameraFollow camFollow;
    private CameraTopDown camTopDown;
    private bool previousCamFollow;
    private bool previousCamPlatformer;


    [SerializeField]
    GameObject gunGO;

    [SerializeField]
    GameObject m_camera;

    // Start is called before the first frame update
    void Start()
    {
        turret = GetComponent<Turret>();
        gun = gunGO.GetComponent<GunLogic>();
        playerController = player.gameObject.GetComponent<PlayerController>();
        camPlatform = m_camera.GetComponent<CameraPlatformer>();
        camFollow = m_camera.GetComponent<CameraFollow>();
        camTopDown = m_camera.GetComponent<CameraTopDown>();
    }

    // Update is called once per frame
    void Update()
    {
        base.Update();
    }

    public override void interact()
    {
        gun.enabled = false;
        turret.enabled = true;
        player.position = transform.position;
        playerController.lockVertical = true;
        playerController.lockHorizontal = true;
        if (camFollow.enabled)
        {
            previousCamFollow = true;
        }
        if (camPlatform.enabled)
        {
            previousCamPlatformer = true;
        }
        camFollow.enabled = false;
        camPlatform.enabled = false;
        camTopDown.enabled = true;
        playerController.lockVertical = true;
        playerController.lockHorizontal = true;
        playerController.lockHeight = true;
        playerController.setVehicle(gameObject);
        base.interact();
    }

    public override void leaveInteraction()
    {
        if (gun != null)
        {
            gun.enabled = true;
        }
        if (turret != null)
        {
            turret.enabled = false;
        }
        if (playerController != null)
        {
            playerController.lockVertical = false;
            playerController.lockHorizontal = false;
            if (previousCamFollow)
            {
                previousCamFollow = false;
                playerController.lockVertical = false;
                camFollow.enabled = true;
            }
            if (previousCamPlatformer)
            {
                previousCamPlatformer = false;
                camPlatform.enabled = true;
            }
            if (camTopDown != null)
            {
                camTopDown.enabled = false;
            }
            playerController.lockHorizontal = false;
            playerController.lockHeight = false;
            playerController.setVehicle(null);
        }
        base.leaveInteraction();
    }

    private void OnDisable()
    {
        leaveInteraction();
    }
}
