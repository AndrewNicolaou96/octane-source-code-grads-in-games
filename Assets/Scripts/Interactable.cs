﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interactable : MonoBehaviour
{


    //max distance player must be to the transform to interact
    public float radius = 5f;
    public bool interacting = false;
    public Transform player;
    private bool interactionEnding;

    void Start()
    {

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }

    public virtual void interact()
    {
        //overridable
        interacting = true;
    }

    public virtual void leaveInteraction()
    {
        //overridable
        interacting = false;
        interactionEnding = true;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        //leave interaction check
        interactionEnding = false;
        if (interacting)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                leaveInteraction();
            }
        }
        float distance = Vector3.Distance(player.position, transform.position);
        //interaction check
        if (distance <= radius)
        {
            //if E pressed and not currently leaving an interaction
            if (Input.GetKeyDown(KeyCode.E) && !interactionEnding)
            {
                interact();
            }
        }
    }

    public void resetTransform()
    {

    }
}
