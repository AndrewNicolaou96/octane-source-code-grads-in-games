﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rain : MonoBehaviour
{
    [SerializeField]
    Transform m_Player;

    // Update is called once per frame
    void Update()
    {
        //follow player position
        transform.position = new Vector3(m_Player.position.x, m_Player.position.y + 10f, m_Player.position.z);
    }

}
